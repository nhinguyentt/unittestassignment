package com.unittest.demo.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unittest.demo.exception.StudentNotFoundException;
import com.unittest.demo.model.Student;
import com.unittest.demo.repository.StudentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
        MediaType.APPLICATION_JSON.getType(),
        MediaType.APPLICATION_JSON.getSubtype(),
        Charset.forName("utf8")
    );

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentRepository studentRepositoryMock;

    @InjectMocks
    private StudentController studentController;

    @Before
    public void init () {
    }


    @Test
    public void findAll_StudentsFound_ShouldReturnFoundStudentEntries () throws Exception {
        Student first  = new Student(1l, "Bob", "A1234567");
        Student second = new Student(2l, "Alice", "B1234568");

        when(studentRepositoryMock.findAll()).thenReturn(Arrays.asList(first, second));

        mockMvc.perform(get("/student"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(2)))
               .andExpect(jsonPath("$[0].id", is(1)))
               .andExpect(jsonPath("$[0].name", is("Bob")))
               .andExpect(jsonPath("$[0].passportNumber", is("A1234567")))
               .andExpect(jsonPath("$[1].id", is(2)))
               .andExpect(jsonPath("$[1].name", is("Alice")))
               .andExpect(jsonPath("$[1].passportNumber", is("B1234568")));

        verify(studentRepositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(studentRepositoryMock);
    }

    @Test
    public void findById_StudentFound_ShouldReturnFoundStudentEntries () throws Exception {
        Student first  = new Student(1l, "Bob", "A1234567");

        when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(first));

        mockMvc.perform(get("/student/{id}", 1))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.name", is("Bob")))
               .andExpect(jsonPath("$.passportNumber", is("A1234567")));

        verify(studentRepositoryMock, times(1)).findById(1L);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void findById_StudentNotFound_ShouldThrowStudentNotFoundException () throws Exception {

        when(studentRepositoryMock.findById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/student/{id}", 1L))
               .andExpect(status().isNotFound());

        verify(studentRepositoryMock, times(1)).findById(1L);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void deleteById_StudentFound_ShouldDeleteStudentEntry () throws Exception {
    	
    	Mockito.doNothing().when(studentRepositoryMock).deleteById(1L);
    	
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete("/student/{id}", 1L))
                .andExpect(status().isOk());
    }
    
    @Test
    public void deleteById_StudentNotFound_ShouldThrowException () throws Exception {
    	
    	 Mockito.doThrow(new StudentNotFoundException("")).when(studentRepositoryMock).deleteById(1L);
    	
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete("/student/{id}", 1L))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void save_ValidStudent_ShouldCreateStudentEntry () throws Exception {
        Student first  = new Student(1l, "Bob", "A1234567");
        
        when(studentRepositoryMock.save(any())).thenReturn(first);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
        								.post("/student")
        								.accept(APPLICATION_JSON_UTF8).content(asJsonString(first))
        								.contentType(APPLICATION_JSON_UTF8);
        mockMvc.perform(requestBuilder)
        					.andExpect(status().is2xxSuccessful());
        
        verify(studentRepositoryMock, times(1)).save(any());
    }
    
    @Test
    public void save_InvalidStudent_ShouldNotCreateStudentEntry () throws Exception {
    	
    	String invalidName = "namewhichhas100charactersnamewhichhas100charactersnamewhichhas100charactersnamewhichhas100charactersnamewhichhas100characters";
    	String invalidPassportNumber = "passwhichhas100characterspasswhichhas100characterspasswhichhas100characterspasswhichhas100characterspasswhichhas100characters";
        Student invalidStudent  = new Student();
        invalidStudent.setId(3l);
        invalidStudent.setName(invalidName);
        invalidStudent.setPassportNumber(invalidPassportNumber);
        
        when(studentRepositoryMock.save(any())).thenReturn(invalidStudent);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
        								.post("/student")
        								.accept(APPLICATION_JSON_UTF8).content(asJsonString(invalidStudent))
        								.contentType(APPLICATION_JSON_UTF8);
        mockMvc.perform(requestBuilder)
        		.andExpect(status().is4xxClientError());
        
        verify(studentRepositoryMock, times(0)).save(any());
    }
    
    @Test
    public void save_ValidUpdateStudent_ShouldUpdateStudentEntry () throws Exception {
        Student student  = new Student(1l, "Bob", "A1234567");
        Student editedStudent = new Student();
        editedStudent.setName("Alice");
        editedStudent.setPassportNumber("B1234568");
        
        when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(student));   
        when(studentRepositoryMock.save(any())).thenReturn(editedStudent);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/student/{id}", 1l)
				.accept(APPLICATION_JSON_UTF8).content(asJsonString(editedStudent))
				.contentType(APPLICATION_JSON_UTF8);
        
        mockMvc.perform(requestBuilder)
        		.andExpect(status().isOk());

        verify(studentRepositoryMock, times(1)).save(any());
    }
    
    @Test
    public void save_InvalidUpdateStudent_ShouldNotUpdateStudentEntry () throws Exception {
        Student student  = new Student(1l, "Bob", "A1234567");
        Student editedStudent = new Student();
      	String invalidName = "namewhichhas100charactersnamewhichhas100charactersnamewhichhas100charactersnamewhichhas100charactersnamewhichhas100characters";
    	String invalidPassportNumber = "passwhichhas100characterspasswhichhas100characterspasswhichhas100characterspasswhichhas100characterspasswhichhas100characters";
        editedStudent.setName(invalidName);
        editedStudent.setPassportNumber(invalidPassportNumber);
        
        when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(student));   
        when(studentRepositoryMock.save(any())).thenReturn(editedStudent);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/student/{id}", 1l)
				.accept(APPLICATION_JSON_UTF8).content(asJsonString(editedStudent))
				.contentType(APPLICATION_JSON_UTF8);
        
        mockMvc.perform(requestBuilder)
        		.andExpect(status().is4xxClientError());

        verify(studentRepositoryMock, times(0)).save(any());
    }
    
    public static String asJsonString (final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

